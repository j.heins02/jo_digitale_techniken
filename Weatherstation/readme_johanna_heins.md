Measurements of Time/Date, Light Intensity, temperature, relative humidity and CO2 concentration

There are 5 different places of the measurements:
    1. on 22/01/04 at 14:41
    2. on 22/01/04 at 17:00
    3. on 22/01/04 at 18:55
    4. on 22/01/05 at 12:30
    5. on 22/01/05 at 15:39
    
Measurements are all taken at different places in the SUB (Niedersächsische Staats- und Universitätsbibliothek)
